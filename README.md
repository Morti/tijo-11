# Prototyp (wzorzec projektowy)
Prototyp – kreacyjny wzorzec projektowy, którego celem jest umożliwienie tworzenia obiektów danej klasy bądź klas z wykorzystaniem już istniejącego obiektu, zwanego prototypem. Głównym celem tego wzorca jest uniezależnienie systemu od sposobu w jaki tworzone są w nim produkty.

#Przykład zastosowania
Omawiany wzorzec stosujemy między innymi wtedy, gdy nie chcemy tworzyć w budowanej aplikacji podklas obiektu budującego (jak to jest w przypadku wzorca fabryki abstrakcyjnej). Wzorzec ten stosujemy podczas stosowania klas specyfikowanych podczas działania aplikacji.

#Konsekwencje stosowania:

###Zalety:
- Redukuje potrzebe podklas.
- Ukrywa skomplikowanie tworzenia objektu.
- Można tworzyć objekty bez wiedzy jakiego typu objekt zostanie stworzony.
- Daje możliwość dodawania oraz usuwania objektów w czasie działania.

###Żródło
- https://www.journaldev.com/1440/prototype-design-pattern-in-java
- https://pl.wikipedia.org/wiki/Prototyp_(wzorzec_projektowy)
- https://www.javatpoint.com/prototype-design-pattern 